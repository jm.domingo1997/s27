const http = require('http');

//Mock Database
let directory = [
	{
		"name" : "Brandon",
		"email" : "brandon@gmail.com"
	},
	{
		"name" : "Jobert",
		"email" : "jobert@gmail.com"
	}
]

const server = http.createServer((req, res)=>{

	//Get all users
	if (req.url == "/users" && req.method == "GET"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		// Input HAS to be data type STRING hence the JSON.stringify method
		//This string input will be converted to desired output data type which has been set to JSON
		// This is done because request and responses sent between client and a node JS server requires the information to be sent and received as a stringified JSON
		res.write(JSON.stringify(directory));
		res.end('Items retrieved from database');
	}


	//POST/Add new user
	if (req.url == "/users" && req.method == "POST"){
		// res.writeHead (200, {'Content-Type': 'text/plain'})
		// res.end('Data to be sent to the database');

		let requestBody = '';

		req.on('data',(data)=>{
			requestBody += data;
		});

		req.on('end',()=>{
			console.log(typeof requestBody);
			requestBody = JSON.parse(requestBody);

			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			};

			directory.push(newUser);
			console.log(directory);

			res.writeHead(200,{'Content-Type':'application/json'});
			res.write(JSON.stringify(newUser));
			res.end();
		})
	}
});

server.listen(3000, 'localhost', () => {
	console.log ('Listening to port 3000')
})